#include <iostream>
#include <cmath>

#include "pluginfactory.hpp"
#include "Controller.h"
#include "ITran.h"


PLUGIN_EXPORT_C
IPluginFactory * GetPluginFactory(ITran *pTran)
{
    PluginFactory * pf = new PluginFactory("plgZSSL", "8.9.0.2");
    pf->RegisterClass<Controller>("Controller");
    pf->Init<Controller>(pTran, "Controller");

    return pf;
}
