CPPFLAGS_SO=g++ -m64 -Wall -lstdc++ -std=c++1y  -pthread -g -O -shared -fPIC
CPPFLAGS=g++ -m64 -Wall -ldl -lstdc++ -std=c++1y  -pthread 
 

all: plgZSSL.so 

#plgZSSL.so: main.cpp Controller.cpp Ctcp.cpp ProcessMsgRequest.cpp ipluginfactory.hpp
#	$(CPPFLAGS_SO) main.cpp Controller.cpp Ctcp.cpp ProcessMsgRequest.cpp -o plgZSSL.so

plgZSSL.so : main.o Controller.o Ctcp.o ProcessMsgRequest.o 
	$(CPPFLAGS_SO) main.o Controller.o Ctcp.o ProcessMsgRequest.o -o plgZSSL.so

main.o: main.cpp
	$(CPPFLAGS) -g -c main.cpp

Controller.o : Controller.cpp
	$(CPPFLAGS) -g -c Controller.cpp

Ctcp.o : Ctcp.cpp
	$(CPPFLAGS) -g -c Ctcp.cpp

ProcessMsgRequest.o : ProcessMsgRequest.cpp
	$(CPPFLAGS) -g -c ProcessMsgRequest.cpp

clean:
		rm -rf *.so
		rm -rf *.o

copy:
	cp plgZSSL.so ./../TRAN_CPP/
		