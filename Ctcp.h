#ifndef _CTCP_H_
#define _CTCP_H_

#include <memory>
#include <thread>
#include "RequestExec.hpp"
#include "QueueManager.hpp"
#include "ProcessMsgRequest.h"

class Ctcp 
{
public:
    Ctcp();

    void OnMessage(const string & message);
private:
    shared_ptr< thread >	            m_MsgReqThreadPtr;
    QueueManager<ProcessMsgRequest>	m_MsgReqQMngr;
};

#endif