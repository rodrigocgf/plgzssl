#pragma once
#include <deque>
#include <vector>
#include <memory>
#include <algorithm>
#include <mutex>
#include <condition_variable>
#include <cassert>

using namespace std;

template<typename T>
class QueueManager 
{
public:
    using DataTypePtr = shared_ptr<T>;
    using QueueType = deque<DataTypePtr>;
    
    QueueManager()
    :m_stop(false)
    {
    }

    void enqueue(DataTypePtr & data)
    {
        {
            unique_lock<mutex> lk(  m_mutex );
            m_queue.push_back(data);
        }
        m_cond.notify_one();
    }
    
    void stop()
    {
        m_stop = true;
        m_cond.notify_all();
    }

    void operator()()
    {
        while ( !m_stop )
        {
            unique_lock<mutex> lk(  m_mutex );
            while ( m_queue.empty() && !m_stop )
                m_cond.wait( lk );
            
            if ( !m_stop)
            {
                assert( lk.owns_lock() );
                assert( m_queue.size() );

                DataTypePtr spExec = m_queue.front();
                assert( spExec.get() );
                m_queue.pop_front();
                lk.unlock();
                (*spExec)();
            }
        }
    }
private:
    QueueType			m_queue;
    mutex		        m_mutex;
    condition_variable	m_cond;
    volatile bool		m_stop;
};
