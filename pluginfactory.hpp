#ifndef _PLUGINFACTORY_HPP_
#define _PLUGINFACTORY_HPP_

#include <string>
#include <functional>
#include <map>
#include <vector>
#include <algorithm>
#include <iostream>
#include "ITran.h"

#include "ipluginfactory.hpp"

// Unix-like OSes
#define PLUGIN_EXPORT_C extern "C" __attribute__ ((visibility ("default")))

/** @brief Concrete implementation of the interface IPluginFactory.
* An instance of this class is supposed to be exported by the plugin
* entry-point function
*/

using namespace std;

class PluginFactory: public IPluginFactory
{

    using CtorItem = std::pair<std::string, std::function<void* ()>>;
    using CtorDB = std::vector<CtorItem>;

    // Constructor map
    using CtorMap = map<string , std::function<void* ()>>;

    /** Plugin name */
    std::string m_name;

    /** Plugin version */
    std::string m_version;  

    CtorDB m_ctordb;
    CtorMap m_ctormap;         

public:
    PluginFactory(const char* name, const char* version):
                            m_name(name),
                            m_version(version)
    {
        cout << "[PluginFactory::PluginFactory] name : " << name << " , version : " << version << endl;
    }

    /** Get Plugin Name */
    const char* Name() const
    {
        return m_name.data();
    }

    /** Get Plugin Version */
    const char* Version() const
    {
        return m_version.data();
    }

    /** Get number of classes exported by the plugin */
    virtual size_t NumberOfClasses() const
    {
        return m_ctordb.size();
    }

    virtual const char* GetClassName(size_t index) const
    {
        return m_ctordb[index].first.data();
    }

    /** Instantiate a class from its name.
    * This member function returns a type-erased pointer
    * to a class object allocated on the heap.
     */
    
    void* Factory(ITran *pTran, const char* className) const
    {
        cout << "[PluginFactory::Factory] class name : " << className << endl;
        auto it = std::find_if(m_ctordb.begin(), m_ctordb.end(),
                                [&className](const CtorItem& p)
                    {
                        return p.first == className;
                    });

        if(it == m_ctordb.end())
            return nullptr;

        return it->second();
    }
    

    /** Register class name and constructor in the plugin database */
    
    template<typename AClass>
    PluginFactory& RegisterClass(std::string const& name)
    {
        cout << "[PluginFactory::RegisterClass] class name : " << name << endl;
        auto constructor = []{ return new (std::nothrow) AClass; };
        m_ctormap.insert(std::make_pair(name, constructor));
        m_ctordb.push_back(std::make_pair(name, constructor));
        return *this;
    }
    
    template<typename AClass>
    void Init(ITran *pTran, string const & name)
    {
        AClass * p = reinterpret_cast<AClass *>(m_ctormap[name]());
        p->Init(pTran);
    }
    
};

#endif