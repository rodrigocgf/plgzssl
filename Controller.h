#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include <memory>
#include <iostream>
#include "Ctcp.h"
#include "ITran.h"


using namespace std;

class Controller 
{
public:
    Controller();
    void Init(ITran *pTran);

    shared_ptr<Ctcp> m_tcpPtr;
};

#endif