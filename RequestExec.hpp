#ifndef _REQUEST_EXEC_HPP
#define _REQUEST_EXEC_HPP

#include "QueueManager.hpp"

template<typename T>
class RequestExec
{
public:
    RequestExec(QueueManager<T> & mngr)
    :m_queueMngr(mngr)
    {
    }
    void operator()() 
    { 
        m_queueMngr();	
    }
private:
    QueueManager<T> &m_queueMngr;
};


#endif