#ifndef _ITRAN_H_
#define _ITRAN_H_

#include <string>

using namespace std;

class ITran 
{
public:
    virtual void OnMsgFromPlugins(const string & message) = 0;
};

#endif