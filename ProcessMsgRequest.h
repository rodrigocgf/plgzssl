#ifndef _PROCESS_MSG_REQUEST_H_
#define _PROCESS_MSG_REQUEST_H_

#include <string>

using namespace std;
class ProcessMsgRequest
{
public:
    ProcessMsgRequest(const string & message): m_message(message)
    {

    }
    ~ProcessMsgRequest(){}

    void operator()();
private:
    string m_message;
};


#endif