#include "Ctcp.h"
#include "ProcessMsgRequest.h"

Ctcp::Ctcp()
{
    m_MsgReqThreadPtr.reset ( new thread( RequestExec<ProcessMsgRequest>( m_MsgReqQMngr ) ) );
}

void Ctcp::OnMessage(const string & message)
{


    shared_ptr<ProcessMsgRequest> processMsgRequestPtr( new ProcessMsgRequest( message ) );

	m_MsgReqQMngr.enqueue ( processMsgRequestPtr );
}